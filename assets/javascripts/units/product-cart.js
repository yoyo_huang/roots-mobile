'use strict';
(function () {

var html_delete_btn = '<div id="delete_btn" class="btn-popup style-gray btn-popup-close"><a>刪除</a></div>';
var html_add_wishlist_btn = '<div id="add_wishlist_btn" class="btn-popup btn-popup-close"><a>加到願望清單</a></div>';
var popupDelete = '<h2 class="tcenter">確定刪除此商品</h2><div class="tcenter">' + html_delete_btn + html_add_wishlist_btn + '</div>';

$("body").on('click', '.do-check-delete', function () {
    $(this).closest(".cart-item").addClass("is-choosing");
    HFeature.popup(popupDelete);
    HFeature.popupOpen();
});

$("body").on('click', '.layout-popup-background,.btn-popup', function () {
    HFeature.popupClose();
});

$("body").on('click', '#delete_btn,#add_wishlist_btn', function () {
    var thisId = $("body").find(".is-choosing").attr("data-id");
    if( $(this).is("#add_wishlist_btn") ){
      alert("add_wishlist");
    }
    $("body").find(".is-choosing").css("width","100vw").animate({ "opacity":"0","margin-left":"100%" },400).slideUp(400).delay(800).queue(
        function(next){
          alert("deleteing: "+thisId);
          $(this).remove();
          getTotalPrice();
          getTotalItem();
        next();
    });
});


function getTotalItem(){
  var totalAmount = 0;
  $(".cart-item").each(function(index) {
    var thisAmount = $(this).find(".input-amount").eq(0).val();
    var regInt = /^[0-9]*[1-9][0-9]*$/; //正整數 return bool 
    //str为你要判斷的字符 執行返回結果 true 或 false
    if(!isNaN(thisAmount) && regInt.test(thisAmount) ){
       // alert("都是數字");
        totalAmount += parseInt( $(this).find(".input-amount").eq(0).val() );
    }else{
      // alert("不全是數字！");
        $(this).find(".input-amount").eq(0).val(0);
        totalAmount += 0 ;
        popAlert("請輸入數字");
    }
    // console.log( totalAmount );
  });
  $(".total-amount").text(totalAmount);
}

function getTotalPrice(){
  var totalPrice = 0;
  $(".cart-item").each(function(index) {
    var thisAmount = $(this).find(".input-amount").eq(0).val();
    var thisPrice = parseInt( $(this).find(".sp-price").eq(0).text() );
    // console.log( thisAmount +","+ thisPrice);
    var regInt = /^[0-9]*[1-9][0-9]*$/; //正整數 return bool
    if(!isNaN(thisAmount) && regInt.test(thisAmount) ){
       // alert("都是數字");
      totalPrice += parseInt( thisAmount * thisPrice );
    }else{
      // alert("不全是數字！");
      $(this).find(".input-amount").eq(0).val(0);
      totalPrice += parseInt( 0 );
      popAlert("請輸入數字");
    }
  });
  $(".total-price").text(totalPrice);
}


getTotalPrice();
getTotalItem();

$(".input-amount").on('blur', function () {
  getTotalPrice();
  getTotalItem();
});

$(".input-amount").keypress(function( event ) {
  if ( event.which == 13 ) {
     event.preventDefault();
    getTotalPrice();
    getTotalItem();
  }
});

$("body").on('click', '.layout-popup-background,.layout-popup-wrap.style-nocolor,.btn-popup', function () {
    HFeature.popupClose();
});



})()

