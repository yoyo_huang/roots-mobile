'use strict';

(function () {
    var innerW = window.innerWidth;
    $("#like-button").on("click", function (element) {
        $(element.target).toggleClass("is-active");
    });

    $(".swiper-slide").on("click", function () {
        // $("#slider-detailImage").toggleClass("is-zoom");
        var currentSlide = $($("#slider-detailImage .swiper-wrapper").children()[swiperMain.activeIndex]);
        var backgroundImage = currentSlide.attr("data-zoomimage");
        // var backgroundImageStr = backgroundImage.substring(5, backgroundImage.length - 2);
        // console.log(backgroundImage, backgroundImageStr);
        var popupContent2 = '<div class="tcenter"><img src="'+backgroundImage+'"></div><i class="icon-close"></i>';
        // $(".layout-popup-wrap .tcenter img").attr("src", backgroundImage);
        HFeature.popup(popupContent2);
        HFeature.popupOpen();
        $(".layout-popup-wrap")
            .addClass("style-nocolor-full")
            .addClass("draggable")
            .find(".tcenter")
            .scrollLeft(innerW/4);
    });

    $("body").on('click', '.layout-popup-background,.layout-popup-wrap.style-nocolor-full,.btn-popup', function () {
        HFeature.popupClose();
    });

    function bindSelectEvent(targetClass, activeClass) {
        $("." + targetClass).on("click", function (element) {
            $("." + targetClass + "." + activeClass).removeClass(activeClass);
            $(element.target).addClass(activeClass);
        });
    }

    bindSelectEvent("btn-sq-size", "btn-sq-size-action");
    bindSelectEvent("btn-sq-color", "btn-sq-color-action");

    $("#purchase-button").on("click", function () {
        popAlert("直接購買");
    });

    $("#add-cart-button").on("click", function () {
        popAlert("放入購物車")
    });

    $(".number-minus").on("click", function () {
        var i = $('#order-number').val();
        if (i != 0) {
            i--;
            $('#order-number').val(i);
        }
    });

    $(".number-add").on("click", function () {
        var i = $('#order-number').val();
        i++;
        $('#order-number').val(i++);
    });

  var headerHeight = hVariable.getHeaderHeight();
  $(".accordion-item").find(".accordion-item-title").off('click');
  $(".accordion-item").find(".accordion-item-title").on('click', function() {
    $(".accordion-item-title").not(this).parent().removeClass("on").find(".fa").removeClass('fa-rotate-180');
    $(this).parent().toggleClass('on').find(".fa").toggleClass('fa-rotate-180');
    
    //only when opening, scroll this item to screen top
    if( $(this).parent().hasClass('on') ){
        var newoffset = parseInt( $(this).offset().top );
        var nowscroll = parseInt( $(".layout-view").scrollTop() );
        $('.layout-view').animate({ scrollTop:( nowscroll + newoffset - headerHeight )}, 300); 
    }

  });
})();