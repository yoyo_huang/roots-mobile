'use strict';
function HCONNECT_FEATURE() {
  var _wrap = "<div class='layout-popup-wrap'></div>",
      _bg = "<div class='layout-popup-background'></div>";
  this.popupWrap = $('<div />');
  this.popupWrap.html(_wrap + _bg);
}
HCONNECT_FEATURE.prototype = {
  popup: function(content) {
    this.popupWrap.find('.layout-popup-wrap').html(content);
  },
  popupOpen: function() {
    var event = jQuery.Event("popupOpen");
    // event.name = "wdjfioejfiejifje"
    $('body')
      .append(this.popupWrap.html())
      .trigger(event)
  },
  popupClose: function() {
    var event = jQuery.Event("popupClose");
    $('body')
      .find('.layout-popup-background, .layout-popup-wrap')
      .remove()
      .end()
      .trigger(event)
  }
}
var HFeature = new HCONNECT_FEATURE();
function HCONNECT_VARIABLE() {
  var _headerHeight = 45,
      _tabbarHeight = 55;

  this.getHeaderHeight = function() {
    return _headerHeight;
  };

  this.getTabbarHeight = function() {
    return _tabbarHeight;
  };
}

var hVariable = new HCONNECT_VARIABLE();

function popAlert(mymsg){
  HFeature.popupClose();  
  var popupContent = '<p class="popup-msg" style="text-align:center;">'+mymsg+'</p>';
  HFeature.popup(popupContent);
  HFeature.popupOpen();
  setTimeout( 'HFeature.popupClose();' , 1500);
};
function popAlertWithOK(mymsg){
  HFeature.popupClose();  
  var popupContent = '<p class="popup-msg" style="text-align:center;">'+mymsg+'</p><div style="text-align:center;"><div class="btn-popup style-short"><a class="btn-ok btn-ok-mypoint">確定</a></div></div>';
  HFeature.popup(popupContent);
  HFeature.popupOpen();
};
(function() {
  // popAlertWithOK("hello!! this is a custom alert with OK button.<br/>test123123");
  // var innerH = window.innerHeight;
  // $(".layout-view").css("height",innerH);
  // 執行 FastClick
  FastClick.attach(document.body);
  //鎖螢幕不能滑
  // document.addEventListener("touchmove", function(event){
  // event.preventDefault();
  // }, false);
})();