'use strict';

(function() {
  //tabbar Circle number
  $(".layout-tabbar-list-link .fa-shopping-cart").attr("data-cartnum","88");

  // Switch Menu
  // $('.do-openmenu').on('click', closeMenu);
$('.do-openmenu').on('click', function(){
  $('body').addClass('layout-menu-open');
});
$('.layout-view,.layout-header,.layout-tabbar-foot').on('click', function(event){
    $('body').removeClass('layout-menu-open');
});

  // Open Sub Menu
  $('.have-sub-item').on('click', '.trigger', function(event) {
    $(this).parent('.have-sub-item').toggleClass('is-active');
    // console.log("ff");
    var _l = $(this).parent('.have-sub-item').find('.layout-menu-sublist').height() > 0 ? 0 : $('.layout-menu-list-subitem').length,
        _h = $(this).parent('.have-sub-item').find('.layout-menu-list-subitem').height();
    
    $(this).parent('.have-sub-item').find('.layout-menu-sublist').css({
      maxHeight: _l * _h + 'px',
    });
    event.preventDefault();
  });
  

  //Open 2nd sub menu
  // $(".have-secondsub-item,.btn-backmainmenu").on("click",function(){
  //   $(".layout-menu-list").toggleClass('is-opening-secondsub');
  // });
$(".have-secondsub-item").each(function(index) {
  $(this).on("click",function(){
    $(".layout-menu-list").addClass('is-opening-secondsub');
    $(".menu-second-sublist").removeClass("is-choosen").eq(index).addClass("is-choosen");
  });  
});

$(".btn-backmainmenu").on("click",function(){
  $(".layout-menu-list").removeClass('is-opening-secondsub');
  $(".menu-second-sublist").removeClass("is-choosen");
});


})()