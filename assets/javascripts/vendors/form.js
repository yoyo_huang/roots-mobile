'use strict';

(function() {
    $("body").on('mousedown',".module-form-input", function(){
      $(this).parent().removeClass("is-error");
      // $(this).blur();
      var nowScroll = $('.layout-view').scrollTop();
      var newScroll = parseInt( $(this).parent().offset().top );
      $('.layout-view').animate({ scrollTop:( nowScroll + newScroll - 45 )}, 300);
      
      $(this).delay(400).queue(
        function(next){
          $(this).focus();
        next();
    });
    });

    $("body").on('click',".module-form-select", function(){
      $(this).removeClass("is-error");
    });

    $("body").on('mousedown',".module-form-textarea", function(){
      var nowScroll = $('.layout-view').scrollTop();
      var newScroll = parseInt( $(this).parent().offset().top );
      $('.layout-view').animate({ scrollTop:( nowScroll + newScroll - 45 )}, 300);
      $(this).parent().removeClass("is-error");
      $(this).focus();
    });
})()