'use strict';

(function() {

  $('.layout-tabbar-foot').on('click','.layout-tabbar-foot-head', function() {
      $('body').toggleClass('is-footer-active');
      $('.layout-tabbar-foot').toggleClass('is-active');      
      $('.layout-tabbar-foot-head i').toggleClass('fa-rotate-180');      
      if($('.layout-tabbar-foot').hasClass('is-active')){
        // alert("OPEN");
        $('.layout-tabbar-foot-head').find("span").text("Back to shopping");
      }else{
        $('.layout-tabbar-foot-head').find("span").text("About Roots");        
      }
  });
  $('.do-goto-top').on('click', function() {
    $('.layout-view').animate({ scrollTop:(0 )}, 300);
  });
  
})()