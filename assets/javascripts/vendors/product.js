'use strict';

(function() {
  $(".product-list").on('click', '.btn-love', function() {
    $(this).toggleClass('liked');
  });

  $(".layout-header-openfilter,.layout-header-endfilter").on('click', function() {
    $("body").toggleClass('filting');
  });
  $(".layout-header-clearfilter").on('click', function() {
    $(".module-filter-list-item input").each(function() {
         $(this).prop("checked", false);
     });           
  });
  $(".module-filter-menu-btn").on('click', function() {
    $(".module-filter-menu-btn.on").toggleClass('on');
    $(this).toggleClass('on');
    if ( $(".module-filter-menu-btn.on").hasClass('tag-filter') ){
        // alert("hi");
        $(".module-filter").addClass("tag-filter").removeClass('order-filter');
    }
    else{
        // alert("bye");
        $(".module-filter").removeClass("tag-filter").addClass('order-filter');      
    }
  });  
})()